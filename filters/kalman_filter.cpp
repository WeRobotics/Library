#include "kalman_filter.h"

KalmanFilter::KalmanFilter(float initial, float covInit)
{
	init(initial, covInit);
}

void KalmanFilter::init(float initial, float covInit)
{
	_x = initial;
	_covariance = covInit;
}

void KalmanFilter::predict(float dt, float vel, float vel_unc)
{
	_x += vel * dt;

	float G = dt;

	float process_noise = G * vel_unc;

	_covariance += process_noise;

}

bool KalmanFilter::update(float meas, float measUnc)
{
	_residual = meas - _x;

	_innovCov = _covariance + measUnc;

	// outlier rejection
	float beta = _residual / _innovCov * _residual;

	// 5% false alarm probability
	if (beta > 3.84f) {
		return false;
	}

	float kalmanGain = _covariance / _innovCov;

	_x += kalmanGain * _residual;

	_covariance = (1.0f - kalmanGain) * _covariance;

	return true;

}

float KalmanFilter::getState()
{
	return _x;
}

float KalmanFilter::getCovariance()
{
	return _covariance;
}

void KalmanFilter::getInnovations(float &innov, float &innovCov)
{
	innov = _residual;
	innovCov = _innovCov;
}
