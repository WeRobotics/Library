#pragma once

//Higly inspired from from PX4 code

//#include <mathlib/mathlib.h>

class KalmanFilter
{
public:

	/**
	 * Default constructor, state not initialized
	 */
	KalmanFilter() {};

	/**
	 * Constructor, initialize state
	 */
	KalmanFilter(float initial, float covInit);

	/**
	 * Default desctructor
	 */
	virtual ~KalmanFilter() {};

	/**
	 * Initialize filter state
	 * @param initial initial state
	 * @param covInit initial covariance
	 */
	void init(float initial, float covInit);

	/**
	 * Predict the state with an external acceleration estimate
	 * @param dt            Time delta in seconds since last state change
	 * @param acc           Acceleration estimate
	 * @param acc_unc       Variance of acceleration estimate
	 */
	void predict(float dt, float vel, float vel_unc);

	/**
	 * Update the state estimate with a measurement
	 * @param meas    state measeasurement
	 * @param measUnc measurement uncertainty
	 * @return update success (measurement not rejected)
	 */
	bool update(float meas, float measUnc);

	/**
	 * Get the current filter state
	 * @retrun x State
	 */
	float getState();

	/**
	 * Get state covariance
	 * @return covariance Covariance of the state
	 */
	float getCovariance();

	/**
	 * Get measurement innovation and covariance of last update call
	 * @param innov Measurement innovation
	 * @param innovCov Measurement innovation covariance
	 */
	void getInnovations(float &innov, float &innovCov);

private:
	float _x; // state

	float _covariance; // state covariance

	float _residual; // residual of last measurement update

	float _innovCov; // innovation covariance of last measurement update
};
