#include "global_include.h"

#include <limits>
#include <cmath>

bool wer::lib::are_equal(float one, float two)
{
    return (std::fabs(one - two) < std::numeric_limits<float>::epsilon());
}

bool wer::lib::are_equal(double one, double two)
{
    return (std::fabs(one - two) < std::numeric_limits<double>::epsilon());
}
