# Log library

This library is inspired by previous logging library. It facilitates logging of simple messages with LogMsg class and logging data with LogData class. Examples are in Hardware/test/log_test.cpp and Hardware/test/log_data_test.cpp.

How to use:\n


For logging messages:\n
  \code{.cpp}
  // fist set the filter
  wer::lib::LogManager::setFilter(wer::lib::LogPriority::DEBUG);
  // initialize either data or text logs by LogManager class
  wer::lib::LogMsg* plog = wer::lib::LogManager::createLogMsg(
      0x00, "path_to_log_file");
   int number=2;
  // after use debug level, but always should end with << '\n'
  plog->warning_<< "test console write 2" << number << '\n';
  plog->error_ << "error" << '\n';
  \endcode

 For logging data:\n

 \code{.cpp}
  //making vectors for column names and data because functions for logging use vectors as parameters
   std::vector<double> vector_test;
  std::vector<std::string> vector_columns_test;
  vector_columns_test.push_back("column1");
  vector_columns_test.push_back("column2");
  vector_columns_test.push_back("column3");
  vector_test.push_back(1.2);
  vector_test.push_back(33.3);
  vector_test.push_back(4.70);

  wer::lib::LogData* plog =
      wer::lib::LogManager::createLogData("/home/vuk/Downloads/log_test_1.csv");
  plog->writeColumnNames(vector_columns_test);
  plog->Data(vector_test, "sensor 1");
  \endcode