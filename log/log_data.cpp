#include <log_data.h>

namespace wer {
namespace lib {

LogData::LogData(std::ofstream* stream, std::string filename) {
  stream_ = stream;
  filename_ = filename;
}
LogData::~LogData() { delete stream_; }

void LogData::writeColumnNames(std::vector<std::string> column_vector) {
  std::lock_guard<std::mutex> guard(log_mutex);
  std::vector<std::string>::iterator it;

  *stream_ << "timestamp,";

  for (it = column_vector.begin(); it != column_vector.end() - 1; ++it) {
    *stream_ << *it;
    *stream_ << ",";
  }
  *stream_ << *it;
  *stream_ << std::endl;
}

std::string LogData::getFilename() { return filename_; }

}  // lib
}  // wer