#ifndef LIBRARY_LOG_LOG_DATA_H
#define LIBRARY_LOG_LOG_DATA_H

#include <sys/types.h>

#include <fstream>
#include <mutex>
#include <string>
#include <vector>

namespace wer {
namespace lib {

extern std::mutex log_mutex;

class LogData {
 public:
  LogData(std::ofstream* stream, std::string filename);
  ~LogData();
  /**
   * [writeColumnNames this will write the name of each column in the order made
   * by column_vector]
   * @param column_vector []
   */
  void writeColumnNames(std::vector<std::string> column_vector);
  /**
   * [Data will write all of data from
   * data_vector, followed by commas,
   * which will make columns in
   * .csv]
   * @param data_vector []
   * @param ID [identification of the data log]
   */
  template <class data_type>
  void Data(std::vector<data_type> data_vector) {
    std::lock_guard<std::mutex> guard(log_mutex);
    time_t rawtime;
    time(&rawtime);
    struct tm* timeinfo = gmtime(&rawtime);
    char time_buffer[20]{};  // We need 8 characters + \0
    strftime(time_buffer, sizeof(time_buffer), "%F %T", timeinfo);

    *stream_ << time_buffer;

    for (auto it = data_vector.begin(); it != data_vector.end(); ++it) {
      *stream_ << ",";
      *stream_ << *it;
    }
    *stream_ << std::endl;
  }

  std::string getFilename();

 private:
  std::ofstream* stream_;
  std::string filename_;
};
}  // namespace lib
}  // namespace wer
#endif  // LIBRARY_LOG_LOG_DATA_H