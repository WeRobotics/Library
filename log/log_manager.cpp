#include "log_manager.h"

#include <fstream>
#include <iostream>
#include <sstream>

namespace wer {
namespace lib {

LogMsg* LogManager::log_msg_console_ = nullptr;
std::vector<LogMsg*> LogManager::log_msg_file_;
std::vector<LogData*> LogManager::log_data_file_;
LogPriority LogManager::filter_log_ = LogPriority::ERROR;

LogMsg* LogManager::createLogMsg(uint8_t console_write,
                                 std::string path_and_file) {
  // checking if a path is an absolute path
  if (path_and_file.at(0) != '/') return nullptr;
  // creating or transfering existing LogMsg class
  LogMsg* log_msg = findFilenameTxt(path_and_file);
  if (log_msg != nullptr) {
    return log_msg;
  } else {
    time_t rawtime;
    time(&rawtime);
    struct tm* timeinfo = gmtime(&rawtime);
    char time_buffer[20]{};  // We need 8 characters + \0
    strftime(time_buffer, sizeof(time_buffer), "%F_%H-%M-%S_UTC",
             timeinfo);
    log_msg =
        new LogMsg(console_write,
                   new std::ofstream(path_and_file + "_" + time_buffer + ".log",
                                     std::ios_base::out | std::ios_base::app),
                   filter_log_, path_and_file);

    log_msg_file_.push_back(log_msg);
    return log_msg;
  }
}

LogData* LogManager::createLogData(std::string path_and_file) {
  if (path_and_file.at(0) != '/') return nullptr;
  LogData* log_data = findFilenameData(path_and_file);
  if (log_data != nullptr) {
    return log_data;
  } else {
    time_t rawtime;
    time(&rawtime);
    struct tm* timeinfo = gmtime(&rawtime);
    char time_buffer[20]{};  // We need 8 characters + \0
    strftime(time_buffer, sizeof(time_buffer), "%F_%H-%M-%S_UTC",
             timeinfo);
    log_data =
        new LogData(new std::ofstream(path_and_file + "_" + time_buffer + ".csv",
                                      std::ios_base::out | std::ios_base::app),
                    path_and_file);

    log_data_file_.push_back(log_data);
    return log_data;
  }
}

LogMsg* LogManager::findFilenameTxt(std::string filename) {
  for (auto it = log_msg_file_.begin(); it != log_msg_file_.end(); ++it) {
    if (filename == (*it)->getFilename()) {
      int index = std::distance(log_msg_file_.begin(), it);
      return log_msg_file_.at(index);
    }
  }
  return nullptr;
}

LogData* LogManager::findFilenameData(std::string filename) {
  for (auto it = log_data_file_.begin(); it != log_data_file_.end(); ++it) {
    if (filename == (*it)->getFilename()) {
      int index = std::distance(log_data_file_.begin(), it);
      return log_data_file_.at(index);
    }
  }
  return nullptr;
}

void LogManager::setFilter(LogPriority filter_log) { filter_log_ = filter_log; }

}  // namespace lib
}  // namespace wer