#ifndef LIBRARY_LOG_LOG_MANAGER_H
#define LIBRARY_LOG_LOG_MANAGER_H

#include <sys/types.h>

#include <iostream>
#include <string>
#include <vector>

#include <log_data.h>
#include <log_msg.h>

namespace wer {
namespace lib {

/**
 * This is class serves as a manager for logging different types of messages.
 */

class LogManager {
 public:
  /**
   * [createLogMsg function is used for creating and obtaining LogMsg
   * class, which is used for writting messages in the file
   * with .txt extension specified as filename and with the possibility to write
   * at the same time on the console
   * If there has already been made a log file with the
   * same filename give as a parameter, this function will return
   * that LogMsg object  ]
   * @param  destination_type [0x01 is to print also to the console, and 0x00 to
   * log only into the filename]
   * @param  path_and_file        [path filename with with absolut file path]
   * @return                  [LogMsg class which can write inside the log file]
   */
  static LogMsg* createLogMsg(uint8_t console_write,
                              std::string path_and_file = "");

  /**
   * [createLogData function is used for creating and obtaining LogData
   * class, which is used for writting messages on the consol or in the file
   * with .csv extension
   * specified as filename. If there has already been made a log file with the
   * same filename give as a parameter, this function will return
   * that LogData object  ]
   * @param  path_and_file [path filename with with absolut file path]
   * @return          [LogData class which can write inside the log file]
   */
  static LogData* createLogData(std::string path_and_file);

  /**
   * [setFilter setting a filter for log files. This will print only logs with
   * higher or equal priorities]
   * @param filter_log [description]
   */
  static void setFilter(LogPriority filter_log);

  ~LogManager();

 private:
  static LogMsg* findFilenameTxt(std::string filename);
  static LogData* findFilenameData(std::string filename);
  static LogMsg* log_msg_console_;
  static std::vector<LogData*> log_data_file_;
  static std::vector<LogMsg*> log_msg_file_;
  static LogPriority filter_log_;
};

}  // lib
}  // wer
#endif  // LIBRARY_LOG_LOG_MANAGER_H