#include <log_msg.h>

#include <mutex>
#include <sstream>

namespace wer {
namespace lib {

std::mutex log_mutex;

LogMsg::LogMsg(uint8_t console_write, std::ofstream* stream,
               LogPriority filter_log, std::string filename)
    : debug_(std::bind(&LogMsg::debug, this, std::placeholders::_1),
             std::bind(&LogMsg::setEndLine, this)),
      error_(std::bind(&LogMsg::error, this, std::placeholders::_1),
             std::bind(&LogMsg::setEndLine, this)),
      warning_(std::bind(&LogMsg::warning, this, std::placeholders::_1),
               std::bind(&LogMsg::setEndLine, this)),
      info_(std::bind(&LogMsg::info, this, std::placeholders::_1),
            std::bind(&LogMsg::setEndLine, this)) {
  console_write_ = console_write;
  file_stream_ = stream;
  console_stream_ = &std::cout;
  filename_ = filename;
  filter_log_ = filter_log;
  line_flag_ = 0x01;
}

void LogMsg::writeMessage(std::string msg, LogPriority log_priority) {
  if (line_flag_ == 0x01) {
    std::stringstream ss;
    time_t rawtime;
    time(&rawtime);
    struct tm* timeinfo = gmtime(&rawtime);
    char time_buffer[20]{};  // We need 8 characters + \0
    strftime(time_buffer, sizeof(time_buffer), "%F %T", timeinfo);

    ss << "[";
    ss << LogPriorityToString(log_priority) << "] ";
    ss << time_buffer << " ";
    // ss << "(";
    // const char* filename_for_debug = __builtin_strrchr(__FILE__, '/')
    //                                      ? __builtin_strrchr(__FILE__, '/') +
    //                                      1 : __FILE__;
    // ss << filename_for_debug;
    // ss << ":" << __LINE__;
    // ss << ") ";
    *file_stream_ << ss.str();
    if (console_write_) {
      setColorPriority(log_priority);
      *console_stream_ << ss.str();
    }
    line_flag_ = 0x00;
  }
  *file_stream_ << msg;
  if (console_write_) {
    *console_stream_ << msg;
  }
}

void LogMsg::setColorPriority(LogPriority priority) {
  switch (priority) {
    case LogPriority::DEBUG:
      *console_stream_ << "\x1b[32m";
      break;
    case LogPriority::INFO:
      *console_stream_ << "\x1b[34m";
      break;
    case LogPriority::WARNING:
      *console_stream_ << "\x1b[31m";
      break;
    case LogPriority::ERROR:
      *console_stream_ << "\x1b[33m";
      break;
  }
}
std::string LogMsg::getFilename() { return filename_; }

void LogMsg::resetColorPriority() { std::cout << "\x1b[0m"; }

LogMsg::~LogMsg() {
  delete file_stream_;
  delete console_stream_;
}

void LogMsg::debug(std::string msg) {
  if (LogPriority::DEBUG >= filter_log_) {
    std::lock_guard<std::mutex> guard(log_mutex);
    writeMessage(msg, LogPriority::DEBUG);
  }
}

void LogMsg::info(std::string msg) {
  if (LogPriority::INFO >= filter_log_) {
    std::lock_guard<std::mutex> guard(log_mutex);
    writeMessage(msg, LogPriority::INFO);
  }
}

void LogMsg::warning(std::string msg) {
  if (LogPriority::WARNING >= filter_log_) {
    std::lock_guard<std::mutex> guard(log_mutex);
    writeMessage(msg, LogPriority::WARNING);
  }
}

void LogMsg::error(std::string msg) {
  if (LogPriority::ERROR >= filter_log_) {
    std::lock_guard<std::mutex> guard(log_mutex);
    writeMessage(msg, LogPriority::ERROR);
  }
}

void LogMsg::setEndLine() {
  line_flag_ = 0x01;
  *file_stream_ << std::endl;
  if (console_write_) *console_stream_ << std::endl;
  resetColorPriority();
}

LogMsgPrint::LogMsgPrint(std::function<void(std::string)> print,
                         std::function<void()> endline) {
  print_ = print;
  endline_ = endline;
}

LogMsgPrint::~LogMsgPrint() {}

LogMsgPrint& LogMsgPrint::operator<<(std::string msg) {
  print_(msg);
  return *this;
}

LogMsgPrint& LogMsgPrint::operator<<(int msg) {
  print_(std::to_string(msg));
  return *this;
}

LogMsgPrint& LogMsgPrint::operator<<(unsigned int msg) {
  print_(std::to_string(msg));
  return *this;
}

LogMsgPrint& LogMsgPrint::operator<<(double msg) {
  print_(std::to_string(msg));
  return *this;
}

void LogMsgPrint::operator<<(char c) {
  if (c == '\n') {
    endline_();
  }
}

}  // namespace lib
}  // namespace wer