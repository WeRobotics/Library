#ifndef LIBRARY_LOG_LOG_MSG_H
#define LIBRARY_LOG_LOG_MSG_H

#include <sys/types.h>

#include <fstream>
#include <functional>
#include <iostream>
#include <string>

namespace wer {
namespace lib {

enum class LogPriority { DEBUG = 0, INFO, WARNING, ERROR };

inline const std::string LogPriorityToString(const LogPriority p) {
  switch (p) {
    case LogPriority::DEBUG:
      return "Debug";
    case LogPriority::INFO:
      return "Info";
    case LogPriority::WARNING:
      return "Warning";
    case LogPriority::ERROR:
      return "Error";
  }
  return "Non existing status";
}

class LogMsgPrint {
 public:
  LogMsgPrint(std::function<void(std::string)> print,
              std::function<void()> endline);
  ~LogMsgPrint();
  LogMsgPrint& operator<<(std::string msg);
  LogMsgPrint& operator<<(int msg);
  LogMsgPrint& operator<<(unsigned int msg);
  LogMsgPrint& operator<<(double msg);
  void operator<<(char c);

 private:
  std::function<void(std::string)> print_;
  std::function<void()> endline_;
};

class LogMsg {
 public:
  LogMsg(uint8_t console_write, std::ofstream* file_stream,
         LogPriority filter_log, std::string filename);

  void debug(std::string msg);
  void info(std::string msg);
  void warning(std::string msg);
  void error(std::string msg);

  void setEndLine();

  LogMsgPrint debug_;
  LogMsgPrint error_;
  LogMsgPrint warning_;
  LogMsgPrint info_;

  ~LogMsg();

  std::string getFilename();

 private:
  /**
* [writeMessageLine will write one line of message in string]
* @param msg []
*/
  void writeMessage(std::string msg, LogPriority log_priority);
  void setColorPriority(LogPriority priority);
  void resetColorPriority();
  std::ofstream* file_stream_;
  std::ostream* console_stream_;
  std::string filename_;
  LogPriority filter_log_;
  uint8_t console_write_;
  uint8_t line_flag_;
};

}  // lib
}  // wer

#endif  // LIBRARY_LOG_LOG_MSG_H