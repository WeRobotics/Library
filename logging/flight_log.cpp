#include "flight_log.h"

#include <ctime>
#include <iomanip>
#include <algorithm>
#include <regex>
#include <experimental/filesystem>

#include <string.h>

#include "log.h"

std::string FlightLog::_logPath("/home/pi/");
std::string FlightLog::_logFileName("flight_log_");
std::string FlightLog::_logFileExt(".csv");
FlightLog* FlightLog::instance;

namespace fs = std::experimental::filesystem;

FlightLog::FlightLog() :
_dronePosition{},
_droneVelocity{},
_gpsOffset{},
_attitude{},
_groundAlt(0.0),
_takeoffPosition{},
_waypointId(0),
_landTargetPosition{},
_landTargetEstiCov{},
_landTargetId(0),
_landImgDelay(0),
_landImageIdx(0),
_landMode(0),
_lidarMeas(0.0),
_cpuTemp(0.0),
_ctrlOutputsp{},
_state(0),
_rssi(0.0f),
_pingMs(0.0f)
{
    std::regex regex(std::string("^flight_log_[0-9]+\\.csv$"));
    uint16_t maxLogId = 0;
    for (const auto & entry : fs::directory_iterator(_logPath)) {

        //LogInfo() << "Evaluating : " << std::string(entry.path().filename());

        if(std::regex_match(std::string(entry.path().filename()), regex)) {
			std::size_t const n = static_cast<std::string>(entry.path().filename()).find_first_of("0123456789");
			std::size_t const m = static_cast<std::string>(entry.path().filename()).find_first_not_of("0123456789", n);

        	//std::string stringNumber = std::regex_replace(entry.path().filename(), std::regex("[^0-9]*([0-9]+).*"), std::string("\\1"));
        	std::string stringNumber = static_cast<std::string>(entry.path().filename()).substr(n, m != std::string::npos ? m-n : m);

        	uint16_t logId = static_cast<uint16_t>(std::atoi(stringNumber.c_str()));
        	maxLogId = logId > maxLogId ? logId : maxLogId;
        }
	}

	_logFile = new std::ofstream(_logPath + _logFileName + std::to_string(maxLogId + 1) +_logFileExt, std::ios_base::out | std::ios_base::app );
	LogInfo() << "Opening log file : " << _logFileName + std::to_string(maxLogId + 1) +_logFileExt;
	writeHeader();
}

FlightLog::~FlightLog() {
	if(_logFile) {
		delete _logFile;
		_logFile = nullptr;
	}
}

void FlightLog::logTelemetry(double position[3], double velocity[3], double gpsOffset[2], double attitude[3], std::string gpsTime) {
	if(_logMtx.try_lock()) {
	    memcpy(_dronePosition, position, (sizeof *position) * 3);
	    memcpy(_droneVelocity, velocity, (sizeof *velocity) * 3);
	    memcpy(_gpsOffset, gpsOffset, (sizeof *gpsOffset) * 2);
	    memcpy(_attitude, attitude, (sizeof *attitude) * 3);
	    _gpsTime = gpsTime;
	    _logMtx.unlock();
	}
}

void FlightLog::logGroundAltitude(double altitude) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

	_groundAlt = altitude;
}

void FlightLog::logTakeoffPosition(double position[3]) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

    memcpy(_takeoffPosition, position, (sizeof *position) * 3);
}

void FlightLog::logMissionStatus(uint8_t waypointId) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

	_waypointId = waypointId;
}

void FlightLog::logPrecisionLanding(double targetPosition[2], float targetCov[6]) {
	if(_logMtx.try_lock()) {
	    memcpy(_landTargetPosition, targetPosition, (sizeof *targetPosition) * 2);
	    memcpy(_landTargetEstiCov, targetCov, (sizeof *targetCov) * 6);
	    _logMtx.unlock();
	}
}

void FlightLog::logPrecisionLandingMode(uint8_t mode) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

	_landMode = mode;
}

void FlightLog::logPrecisionLandingMeasurment(uint8_t targetId, uint8_t delay, uint8_t imageIdx) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

	_landTargetId = targetId;
	_landImageIdx = imageIdx;
	_landImgDelay = delay;
}

void FlightLog::logLidar(double measurment) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

    _lidarMeas = measurment;
}

void FlightLog::logCpuTemp(double temperature) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

    _cpuTemp = temperature;
}

void FlightLog::logConnectedStation(uint8_t stationId, bool connected) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

	auto it = std::find(_connectedStations.begin(), _connectedStations.end(), stationId);
	if(it != _connectedStations.end() && connected == false) {
		_connectedStations.erase(it);
	}
	else if(it == _connectedStations.end() && connected) {
		_connectedStations.push_back(stationId);
	}
}

void FlightLog::logSetpoint(double setpoint[4]) {
	if(_logMtx.try_lock()) {
		memcpy(_ctrlOutputsp, setpoint, (sizeof *setpoint) * 4);
		_logMtx.unlock();
	}
}

void FlightLog::logFlightMode(uint8_t state) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

    _state = state;
}

void FlightLog::logRadioStatus(float rssi) {
	std::lock_guard<std::mutex> lock_t(_logMtx);

    _rssi = rssi;
}

void FlightLog::logPingStatus(uint8_t stationId, float pingMs) {
    _pingMs = pingMs;
}

void FlightLog::writeHeader() {
	std::lock_guard<std::mutex> lock_t(_logMtx);

	if(_logFile) {
		*_logFile << "timestamp;lat;lon;alt;gps_time;vx;vy;vz;lat_of;lon_of;att_x;att_y;att_z;"
				  << "gnd_alt;to_lat;to_lon;to_alt;wp_id;"
				  << "tar_pos_x;tar_pos_y;tar_cov_1x;tar_cov_1y;tar_cov_2x;tar_cov_2y;tar_cov_3x;tar_cov_3y;tar_id;"
				  << "land_mode;land_img;land_img_delay;"
				  << "lidar;cpu_temp;stations;sp_x;sp_y;sp_z;sp_yaw;state;rssi;pingMs"
				  << std::endl;
	}
}

void FlightLog::cycle() {
	time_t rawtime;
	time(&rawtime);
	struct tm *timeinfo = localtime(&rawtime);
	char time_buffer[10]{}; // We need 8 characters + \0
	strftime(time_buffer, sizeof(time_buffer), "%I:%M:%S", timeinfo);

	_logMtx.lock();
	std::string serializedStations("");
	for(auto it = _connectedStations.begin(); it != _connectedStations.end(); it++) {
		serializedStations += std::to_string(unsigned(*it)) + ",";
	}

	if(_logFile) {
		*_logFile << time_buffer << ";" 
				  << std::setprecision(9)
				  << _dronePosition[0] << ";" << _dronePosition[1] << ";" << _dronePosition[2] << ";"
				  << std::setprecision(7)
				  << _gpsTime
				  << _droneVelocity[0] << ";" << _droneVelocity[1] << ";" << _droneVelocity[2] << ";"
				  << _gpsOffset[0] << ";" << _gpsOffset[1] << ";"
				  << _attitude[0] << ";" << _attitude[1] << ";" << _attitude[2] << ";" << _groundAlt 
				  << std::setprecision(9)
				  << ";" << _takeoffPosition[0] << ";" << _takeoffPosition[1] << ";" << _takeoffPosition[2] << ";"
				  << std::setprecision(7)
				  << ";" << unsigned(_waypointId) << ";" << _landTargetPosition[0] << ";" << _landTargetPosition[1] << ";"
				  << _landTargetEstiCov[0] << ";" << _landTargetEstiCov[1] << ";" << _landTargetEstiCov[2] << ";" 
				  << _landTargetEstiCov[3] << ";" << _landTargetEstiCov[4] << ";" << _landTargetEstiCov[5] << ";"
				  << unsigned(_landTargetId) << ";" << unsigned(_landMode) << ";" << unsigned(_landImageIdx) << ";" << unsigned(_landImgDelay) << ";" 
				  << _lidarMeas << ";" << _cpuTemp << ";" << serializedStations << ";"
				  << _ctrlOutputsp[0] << ";" << _ctrlOutputsp[1] << ";" << _ctrlOutputsp[2] << ";" << _ctrlOutputsp[3] << ";"
				  << unsigned(_state) << ";" << unsigned(_rssi) << ";" << unsigned(_pingMs)
				  << std::endl;
	}
	_logMtx.unlock();
}