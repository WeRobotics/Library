#pragma once

#include <iostream>
#include <fstream>

#include <vector>
#include <mutex>

class FlightLog {
  public:
    static FlightLog* getInstance(){
      if (!instance){
          instance = new FlightLog();
      }
      return instance;
    };
    FlightLog(FlightLog const&)   = delete;
    void operator=(FlightLog const&)  = delete;
  private:
    static FlightLog* instance;
    FlightLog();
    ~FlightLog();

  public:
    void cycle();

    void logTelemetry(double position[3], double velocity[3], double gpsOffset[2], double attitude[3], std::string gpsTime);
    void logGroundAltitude(double altitude);
    void logTakeoffPosition(double position[3]);
    void logMissionStatus(uint8_t waypointId);
    void logPrecisionLanding(double targetPosition[2], float targetCov[6]);
    void logPrecisionLandingMode(uint8_t mode);
    void logPrecisionLandingMeasurment(uint8_t targetId, uint8_t delay, uint8_t imageIdx);
    void logLidar(double measurment);
    void logCpuTemp(double temperature);

    void logConnectedStation(uint8_t stationId, bool connected);
    void logSetpoint(double setpoint[4]);
    void logFlightMode(uint8_t state);
    void logRadioStatus(float rssi);
    void logPingStatus(uint8_t stationId, float pingMs);

	private:
    static std::string _logPath;
    static std::string _logFileName;
    static std::string _logFileExt;

    double _dronePosition[3];
    double _droneVelocity[3];
    double _gpsOffset[2];
    double _attitude[3];
    std::string _gpsTime;
    double _groundAlt;
    double _takeoffPosition[3];
    uint8_t _waypointId;
    double _landTargetPosition[2];
    float _landTargetEstiCov[6];
    uint8_t _landTargetId;
    uint8_t _landImgDelay;
    uint8_t _landImageIdx;
    uint8_t _landMode;
    double _lidarMeas;
    double _cpuTemp;
    std::vector<uint8_t>  _connectedStations;
    double _ctrlOutputsp[4];
    uint8_t _state;
    float _rssi;
    float _pingMs;

    std::ofstream* _logFile;
    std::mutex _logMtx;

    void writeHeader();
};
