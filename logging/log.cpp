#include "log.h"
#include <fstream>
#include <mutex>

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_GRAY "\x1b[37m"
#define ANSI_COLOR_RESET "\x1b[0m"

std::ofstream* log_file;
std::ofstream* radio_file;
std::ofstream* landing_file;
std::ofstream* message_file;
std::ofstream* sensor_file;

std::function<void(std::string, LogLevel)> _callback;

std::mutex logging_mtx;

void set_color(Color color)
{
    switch (color) {
        case Color::RED:
            std::cout << ANSI_COLOR_RED;
            break;
        case Color::GREEN:
            std::cout << ANSI_COLOR_GREEN;
            break;
        case Color::YELLOW:
            std::cout << ANSI_COLOR_YELLOW;
            break;
        case Color::BLUE:
            std::cout << ANSI_COLOR_BLUE;
            break;
        case Color::GRAY:
            std::cout << ANSI_COLOR_GRAY;
            break;
        case Color::RESET:
            std::cout << ANSI_COLOR_RESET;
            break;
    }
}

void open_log(std::string path) {
   
    
    if(log_file == NULL) {
        logging_mtx.lock();
        log_file = new std::ofstream(path + "/log_file.txt", std::ios_base::out | std::ios_base::app );
        radio_file = new std::ofstream(path + "/radio_file.txt", std::ios_base::out | std::ios_base::app );
        landing_file = new std::ofstream(path + "/land_file.txt", std::ios_base::out | std::ios_base::app );
        message_file = new std::ofstream(path + "/msg_file.txt", std::ios_base::out | std::ios_base::app );
        sensor_file = new std::ofstream(path + "/sensor_file.txt", std::ios_base::out | std::ios_base::app );
        logging_mtx.unlock();
    }
}

void close_log() {

    if(log_file != NULL) {

        logging_mtx.lock();
        delete log_file;
        delete radio_file;
        delete landing_file;
        delete message_file;
        delete sensor_file;
        log_file = NULL;
        radio_file = NULL;
        landing_file = NULL;
        message_file = NULL;
        sensor_file = NULL;
        logging_mtx.unlock();
    }
}

void write_log_radio(std::string message) {
	if(radio_file != NULL) {
        std::lock_guard<std::mutex> lock_t(logging_mtx);

		*radio_file << message << std::endl;
	}
}

void write_log_landing(std::string message){
    if(landing_file != NULL) {
        std::lock_guard<std::mutex> lock_t(logging_mtx);

        *landing_file << message << std::endl;
    }
}

void write_log_message(std::string message){
    if(message_file != NULL) {
        std::lock_guard<std::mutex> lock_t(logging_mtx);

        *message_file << message << std::endl;
    }
}

void write_log_sensor(std::string message){
    if(sensor_file != NULL) {
        std::lock_guard<std::mutex> lock_t(logging_mtx);

        *sensor_file << message << std::endl;
    }
}

void write_log(std::string message, std::string shortMessage, LogLevel level) {
    if(log_file != NULL) {
        std::lock_guard<std::mutex> lock_t(logging_mtx);

        *log_file << message << std::endl;
    }

    if(_callback && level != LogLevel::Info) {
        _callback(shortMessage, level);
    }
}
void set_log_callback(std::function<void(std::string, LogLevel)> callback) {
    _callback = callback;
}

