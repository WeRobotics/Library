#pragma once

// Higly inspred (and copy paste) from dronecore logging library

#include <sstream>

#include <iostream>
#include <ctime>
#include <string>
#include <functional>

#define __FILENAME__ \
    (__builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/') + 1 : __FILE__)


// For release builds, don't show debug printfs, and just discard it into the NullStream.
class NullStream {
public:
    template<typename TPrintable> NullStream &operator<<(TPrintable const &)
    {
        /* no-op */
        static NullStream nothing;
        return nothing;
    }
};
/*
#if DEBUG
#define LogDebug() LogDebugDetailed(__FILENAME__, __LINE__)
#else
#define LogDebug() NullStream()
#endif
*/

#define LogInfo() LogInfoDetailed(__FILENAME__, __LINE__)
#define LogWarn() LogWarnDetailed(__FILENAME__, __LINE__)
#define LogErr() LogErrDetailed(__FILENAME__, __LINE__)

#define LogRadio() LogRadioDetailed(__FILENAME__, __LINE__)
#define LogLanding() LogLandingDetailed(__FILENAME__, __LINE__)
#define LogMessage() LogMessageDetailed(__FILENAME__, __LINE__)
#define LogSensor() LogSensorDetailed(__FILENAME__, __LINE__)

enum class Color { RED, GREEN, YELLOW, BLUE, GRAY, RESET };
enum LogLevel { Debug, Info, Warn, Err, Radio, Landing, Message, Sensor};

void set_color(Color color);
void open_log(std::string path);
void close_log();

void write_log(std::string message, std::string shortMessage, LogLevel level);
void write_log_radio(std::string message);
void write_log_landing(std::string message);
void write_log_message(std::string message);
void write_log_sensor(std::string message);
void set_log_callback(std::function<void(std::string, LogLevel)> callback);

class LogDetailed {
public:
    LogDetailed(const char *filename, int filenumber) :
        _s(),
        _caller_filename(filename),
        _caller_filenumber(filenumber)
    {}

    template<typename T> LogDetailed &operator<<(const T &x)
    {
        _s << x;
        return *this;
    }

    virtual ~LogDetailed()
    {
	// Time output taken from:
        // https://stackoverflow.com/questions/16357999#answer-16358264
        time_t rawtime;
        time(&rawtime);
        struct tm *timeinfo = localtime(&rawtime);
        char time_buffer[10]{}; // We need 8 characters + \0
        strftime(time_buffer, sizeof(time_buffer), "%I:%M:%S", timeinfo);

	if(_log_level != LogLevel::Radio && _log_level != LogLevel::Landing 
        && _log_level != LogLevel::Message && _log_level != LogLevel::Sensor) {
	        switch (_log_level) {
	            case LogLevel::Debug:
	                set_color(Color::GREEN);
	                break;
	            case LogLevel::Info:
	                set_color(Color::BLUE);
	                break;
	            case LogLevel::Warn:
	                set_color(Color::YELLOW);
	                break;
	            case LogLevel::Err:
	                set_color(Color::RED);
	                break;
		    default:
		    break;
	        }

	        std::string message;
	        std::cout << "[" << time_buffer;
	        message = "[" + std::string(time_buffer);

        	switch (_log_level) {
        	    case LogLevel::Debug:
        	        std::cout << "|Debug] ";
        	        message += "|Debug] ";
        	        break;
        	    case LogLevel::Info:
        	        std::cout << "|Info ] ";
        	        message += "|Info ] ";
        	        break;
        	    case LogLevel::Warn:
        	        std::cout << "|Warn ] ";
        	        message += "|Warn ] ";
        	        break;
        	    case LogLevel::Err:
        	        std::cout << "|Error] ";
        	        message += "|Error] ";
        	        break;
		   default:
		   break;
        	}

        	set_color(Color::RESET);

	        std::cout << _s.str();
		std::cout << " (" << _caller_filename << ":" << _caller_filenumber << ")";

	        message += std::string(_s.str());
		std::string shortMessage = std::string(_s.str());
        	write_log(message, shortMessage, _log_level);

	        std::cout << std::endl;
	}
	else {
        std::string  message;
        switch (_log_level) {
            case LogLevel::Radio:
                message = "[" + std::string(time_buffer) + "] " + std::string(_s.str());
                write_log_radio(message);
            break;

            case LogLevel::Landing:
                message = "[" + std::string(time_buffer) + "] " + std::string(_s.str());
                write_log_landing(message);
            break;

            case LogLevel::Message:
                message = "[" + std::string(time_buffer) + "] " + std::string(_s.str());
                write_log_message(message);
            break;

            case LogLevel::Sensor:
                message = "[" + std::string(time_buffer) + "] " + std::string(_s.str());
                write_log_sensor(message);
            break;

            default:
            break;
        }
	}
    }

protected:
    LogLevel _log_level;

private:
    std::stringstream _s;
    const char *_caller_filename;
    int _caller_filenumber;
};

class LogDebugDetailed : public LogDetailed {
public:
    LogDebugDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Debug;
    }
};

class LogInfoDetailed : public LogDetailed {
public:
    LogInfoDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Info;
    }
};

class LogWarnDetailed : public LogDetailed {
public:
    LogWarnDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Warn;
    }
};

class LogErrDetailed : public LogDetailed {
public:
    LogErrDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Err;
    }
};

class LogRadioDetailed : public LogDetailed {
public:
    LogRadioDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Radio;
    }
};

class LogLandingDetailed : public LogDetailed {
public:
    LogLandingDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Landing;
    }
};

class LogMessageDetailed : public LogDetailed {
public:
    LogMessageDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Message;
    }
};

class LogSensorDetailed : public LogDetailed {
public:
    LogSensorDetailed(const char *filename, int filenumber) : LogDetailed(filename, filenumber)
    {
        _log_level = LogLevel::Sensor;
    }
};
