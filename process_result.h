#pragma once

namespace wer {
    namespace lib {

        enum class ProcessResult {
            SUCCESS = 0,
            TIMEOUT,
            NOT_IMPLEMENTED,
            PARSING_ERROR,
            INVALID_ARGUMENTS,
            NOT_CONNECTED,
            UNKNOWN
        };

        inline const char* processResultStr(const ProcessResult result)
        {
            switch (result) {
                case ProcessResult::SUCCESS:
                    return "Success";
                case ProcessResult::TIMEOUT:
                    return "Timeout";
                case ProcessResult::PARSING_ERROR:
                    return "Parsing error";
                case ProcessResult::NOT_IMPLEMENTED:
                    return "Not implemented";
                case ProcessResult::INVALID_ARGUMENTS:
                    return "Invalid arguments";
                case ProcessResult::NOT_CONNECTED:
                    return "Not connected";
                default:
                    return "Unknown";
            }
        }
    }
}