#include "time_handler.h"

#include <cfloat>
#include <cstdint>
#include <thread>

using std::chrono::steady_clock;

wer::lib::Time::Time() {}
wer::lib::Time::~Time() {}

wer::lib::TimePoint wer::lib::Time::steady_time()
{
    return steady_clock::now();
}

double wer::lib::Time::elapsed_s()
{
    auto now = steady_time().time_since_epoch();

    return (now.count()) * steady_clock::period::num /
           static_cast<double>(steady_clock::period::den);
}

double wer::lib::Time::elapsed_since_s(const wer::lib::TimePoint &since)
{
    auto now = steady_time();

    return ((now - since).count()) * steady_clock::period::num /
           static_cast<double>(steady_clock::period::den);
}

wer::lib::TimePoint wer::lib::Time::steady_time_in_future(double duration_s)
{
    auto now = steady_time();
    return now + std::chrono::milliseconds(int64_t(duration_s * 1e3));
}

void wer::lib::Time::shift_steady_time_by(wer::lib::TimePoint &time, double offset_s)
{
    time += std::chrono::milliseconds(int64_t(offset_s * 1e3));
}

void wer::lib::Time::sleep_for(std::chrono::hours h)
{
    std::this_thread::sleep_for(h);
}

void wer::lib::Time::sleep_for(std::chrono::minutes m)
{
    std::this_thread::sleep_for(m);
}

void wer::lib::Time::sleep_for(std::chrono::seconds s)
{
    std::this_thread::sleep_for(s);
}

void wer::lib::Time::sleep_for(std::chrono::milliseconds ms)
{
    std::this_thread::sleep_for(ms);
}

void wer::lib::Time::sleep_for(std::chrono::microseconds us)
{
    std::this_thread::sleep_for(us);
}

void wer::lib::Time::sleep_for(std::chrono::nanoseconds ns)
{
    std::this_thread::sleep_for(ns);
}

wer::lib::FakeTime::FakeTime() : Time()
{
    // Start with current time so we don't start from 0.
    _current = steady_clock::now();
}

wer::lib::FakeTime::~FakeTime() {}

wer::lib::TimePoint wer::lib::FakeTime::steady_time()
{
    return _current;
}

void wer::lib::FakeTime::sleep_for(std::chrono::hours h)
{
    _current += h;
    add_overhead();
}

void wer::lib::FakeTime::sleep_for(std::chrono::minutes m)
{
    _current += m;
    add_overhead();
}

void wer::lib::FakeTime::sleep_for(std::chrono::seconds s)
{
    _current += s;
    add_overhead();
}

void wer::lib::FakeTime::sleep_for(std::chrono::milliseconds ms)
{
    _current += ms;
    add_overhead();
}

void wer::lib::FakeTime::sleep_for(std::chrono::microseconds us)
{
    _current += us;
    add_overhead();
}

void wer::lib::FakeTime::sleep_for(std::chrono::nanoseconds ns)
{
    _current += ns;
    add_overhead();
}

void wer::lib::FakeTime::add_overhead()
{
    _current += std::chrono::microseconds(50);
}