#pragma once

#include <chrono>

namespace wer {
    namespace lib {

        typedef std::chrono::time_point<std::chrono::steady_clock> TimePoint;

        class Time {
        public:
            Time();
            virtual ~Time();

            virtual TimePoint steady_time();
            double elapsed_s();
            double elapsed_since_s(const TimePoint &since);
            TimePoint steady_time_in_future(double duration_s);
            void shift_steady_time_by(TimePoint &time, double offset_s);

            virtual void sleep_for(std::chrono::hours h);
            virtual void sleep_for(std::chrono::minutes m);
            virtual void sleep_for(std::chrono::seconds s);
            virtual void sleep_for(std::chrono::milliseconds ms);
            virtual void sleep_for(std::chrono::microseconds us);
            virtual void sleep_for(std::chrono::nanoseconds ns);
        };

        class FakeTime : public Time {
        public:
            FakeTime();

            virtual ~FakeTime();
            virtual TimePoint steady_time() override;
            virtual void sleep_for(std::chrono::hours h) override;
            virtual void sleep_for(std::chrono::minutes m) override;
            virtual void sleep_for(std::chrono::seconds s) override;
            virtual void sleep_for(std::chrono::milliseconds ms) override;
            virtual void sleep_for(std::chrono::microseconds us) override;
            virtual void sleep_for(std::chrono::nanoseconds ns) override;

        private:
            std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> _current{};
            void add_overhead();
        };

        bool are_equal(float one, float two);
        bool are_equal(double one, double two);
    }
}
