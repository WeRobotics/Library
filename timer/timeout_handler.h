#pragma once

// Higly inspred (and copy paste) from dronecore logging library

#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <thread>

#include "time_handler.h"

namespace wer {
namespace lib {

class TimeoutHandler {
 public:
  TimeoutHandler(wer::lib::Time &time);
  ~TimeoutHandler();

  // delete copy and move constructors and assign operators
  TimeoutHandler(TimeoutHandler const &) = delete;             // Copy construct
  TimeoutHandler(TimeoutHandler &&) = delete;                  // Move construct
  TimeoutHandler &operator=(TimeoutHandler const &) = delete;  // Copy assign
  TimeoutHandler &operator=(TimeoutHandler &&) = delete;       // Move assign

  void add(std::function<void()> callback, double duration_s, void **cookie);
  void refresh(const void *cookie);
  void remove(const void *cookie);

  void run_once();

 private:
  struct Timeout {
    std::function<void()> callback;
    wer::lib::TimePoint time;
    double duration_s;
  };

  std::map<void *, std::shared_ptr<Timeout>> _timeouts{};
  std::mutex _timeouts_mutex{};
  bool _iterator_invalidated{false};

  wer::lib::Time &_time;
};

}  // namespace lib
}  // namespace wer
